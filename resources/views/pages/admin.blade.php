@extends('app')

@section('content')
<div class="col-md-8 content-right" style="border-right: 1px solid #e6e6e6; border-top: 1px solid #e6e6e6;">
	<div class="content-right-top">
		<h5 class="head">Posts</h5>
		@foreach($articles as $article)
			<!-- <a href="{{ url('/articles', $article->id) }}"> -->
				<div class="content">
					<div class="col-md-8 content-left">
						<div class="article">
							<h5 class="head" style="color: yellow">{{ $article->title }}</h5>
							<img src="uploads/{{$article->image_path}}" alt="$latest" class="img-responsive"/>
						</div>
					</div>
					<div class="col-md-4 content-right">
						<a href="{{ url('/articles/edit', [$article->id]) }}">
							<input type="button" value="Edit Post" class="btn btn-primary" />
						</a>
						{!! Form::open(['method' =>'DELETE', 'route' => ['articles.destroy', $article->id]]) !!}
							{!! Form::hidden('_method', 'DELETE') !!}
							{!! Form::submit('Delete Post', array('class' => 'btn btn-danger')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			<!-- </a> -->
		@endforeach
	</div>
</div>

<div class="col-md-4 content-right container" style="border-top: 1px solid #e6e6e6;">
	<div class="content-right-top">
		<h5 class="head">Control Panel</h5>
		<div class="content">
			<h4>Add and Delete Tags</h4>
			
			
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/tags') }}">
					{!! csrf_field() !!}
					<div class="">
						<input type="text" name="name" id="name" class="form-control" style="width: 50%" placeholder="Add a Tag"/>
						<input type="submit" class="btn btn-primary" value="Add" />
					</div>
				</form>
				
				@include('errors.list')
			
			
			{!! Form::open(['method' =>'POST', 'url' => '/deleteTag']) !!}
				{!! Form::hidden('_method', 'POST') !!}
				
				{!! Form::label('tag_lists', 'Tags:(Select a tag to delete)') !!}
				{!! Form::select('tag_list[]', $tags, null, ['id' => 'tag_list', 'class' => 'form-control js-example-placeholder-single']) !!}
				
				{!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
			{!! Form::close() !!}
		</div>
	</div>
</div>
<script type="text/javascript">
		$('#tag_list').select2({
			placeholder: "Select a Tag to Delete",
			allowClear: true
		});
	</script>
@stop