@extends('app')

@section('content')

	<!-- INSTAGRAM PROFILE THEME -->
	
	<header>
      	<img src="{{ URL::to('/') }}/images/bk.jpg" alt="Mountains">
      	<div class="name fancy-font">
        	{{ $user->name }}
            
      	</div>
      	<div class="titles">
          	<h1>Hello! <span>I'm {{ $user->name }}</span></h1>
          	<h2>I love to travel all around the world and design beautiful things</h2>
      	</div>
      	<div class="social">
      		
          	<!--
          	<a class="facebook" href="#">Facebook</a>
          	<a class="twitter" href="#">Twitter</a>
          	<a class="instagram" href="#">Instagram</a>
          	 -->
      	</div>
    </header>

	<section class="instagram-wrap">
      	<div class="container">
	        <div class="row">
            	<div class="col-xs-12">
	                <div class="instagram-content">
                    	<h3>Latest Photos</h3>
                    	<div class="row photos-wrap">
                    	<!-- Instafeed target div -->
                    		<div id="instafeed"></div>
                    		<!-- The following HTML will be our template inside instafeed -->
                    		@foreach($articles as $article)
                    			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        			<div class="photo-box">
	                            		<div class="image-wrap">
                                			<img src="{{ URL::to('/') }}/uploads/{{$article->image_path}}">
                                			<div class="likes">309 Likes</div>
                            			</div>
                            			<div class="description">
	                                		{{ $article->title }}
                                			<div class="date">{{ $article->published_at }}</div>
                            			</div>
                        			</div>
                    			</div>
                    		@endforeach
                    		
                		</div>
              		</div>
            	</div>
          	</div>
        </div>
    </section>

	<!-- <div class="container">
		<div class="row">
			<div class="col-md-8 content-right" style="border-right: 1px solid #e6e6e6; border-top: 1px solid #e6e6e6;">
				<div class="content-right-top">
					<h5 class="head" style="background-color: yellow; color: red;">{{ Auth::user()->name }}</h5>
					@if($articles)
					
					@foreach($articles as $article)
						<div class="content" style="border-bottom: 1px solid #e6e6e6">
							<div class="col-md-7 content-left">
								<div class="article">
									<h5 class="head" style="color: yellow">{{ $article->title }}</h5>
									<img src="uploads/{{$article->image_path}}" alt="$latest" class="img-responsive"/>
									
								</div>
							</div>
							<div class="col-md-5">
								<a href="{{ url('/articles/edit', [$article->id]) }}">
									<input type="button" value="Edit Post" class="btn btn-primary" />
								</a>
								{!! Form::open(['method' =>'DELETE', 'route' => ['articles.destroy', $article->id]]) !!}
									{!! Form::hidden('_method', 'DELETE') !!}
									{!! Form::submit('Delete Post', array('class' => 'btn btn-danger')) !!}
								{!! Form::close() !!}
							</div>
						</div>
					@endforeach
				@else
					<p>There are no Posts right now!!!!</p>
				@endif
				</div>
						
			</div>
			<div class="col-md-4">
				<a href="{{ url('/articles/create') }}">
					<input type="button" value="Create Post" />
				</a>
			</div>
		</div>
		
	</div> -->
@stop