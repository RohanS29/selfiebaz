@extends('app')

@section('content')
	<div class="container">
		<h5 class="head" style="background-color: yellow; color: red;">timeline</h5>
			@foreach($articles as $article)
				
				<div class="content">
					<div class="col-md-7 content-left">
						<div class="article">
							<a href="{{ url('/profiles', $article->user->id) }}" style="text-decoration: none;">
								<h5 class="head" style="color: yellow">{{ $article->user->name }}</h5>
							</a>
							<p>{{ $article->title }}</p>
							<img src="uploads/{{$article->image_path}}" alt="$latest" class="img-responsive"/>
						</div>
						<div class="panel-body">
							<form method="post" action="{{ url('/comment/add') }}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="on_post" value="{{ $article->id }}">
								
								<div class="form-group">
									<textarea required="required" placeholder="Enter comment here" style="resize: vertical" name = "body" class="form-control"></textarea>
								</div>
								<input type="submit" name='post_comment' class="btn btn-success" value = "Post"/>
							</form>
						</div>
						<div>
							@if($comments)
								<ul style="list-style: none; padding: 0">
									@foreach($comments as $comment)
										@if($article->id == $comment->article_id)
										<li class="panel-body">
											<div class="list-group">
												<div class="list-group-item">
													<a href="{{ url('/profiles', $comment->user->id) }}" style="text-decoration: none;">
														<h3>{{ $comment->user->name }}</h3>
													</a>
													<p>{{ $comment->created_at->format('M d,Y \a\t h:i a') }}</p>
												</div>
												<div class="list-group-item">
													<p>{{ $comment->comment }}</p>
												</div>
											</div>
										</li>
										@endif
									@endforeach
								</ul>
							@endif
						</div>
					</div>
				</div>
				
			@endforeach
		
	</div>
@stop