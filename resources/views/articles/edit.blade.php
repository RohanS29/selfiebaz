

@extends('app')

@section('content')

	<h1>Edit: {!! $article->title !!}</h1>
	
	{!! Form::model($article, ['method' =>'PATCH', 'action' => ['ArticlesController@update', $article->id],'file' => 'true', 'enctype' => 'multipart/form-data']) !!}
		<div class="form-group">
			{!! Form::label('title', 'Title:') !!}
			{!! Form::text('title', null, ['class' => 'form-control']) !!}
		</div>

		@if($article['image_path'])
			<img src="../../uploads/{{$article->image_path}}" alt="$article" class="img-responsive"/>
		@endif

		<div class="form-group">
			{!! Form::label('Upload Image', '', ['class' => 'control-label']) !!}
			<input type="file" id="image_path" name="image_path" class="file" accept="image/*" />
		</div>
	
		<!-- <div class="form-group">
			{!! Form::label('body', 'Body:') !!}
			{!! Form::textarea('body', null, ['class' => 'form-control']) !!}
		</div> -->
		
		<!-- <div class="form-group">
			{!! Form::label('published_at', 'Publish On:') !!}
			{!! Form::input('date', 'published_at', date('Y-m-d'), ['class' => 'form-control']) !!}
		</div> -->
	
		<div class="form-group">
			{!! Form::label('tag_lists', 'Tags:') !!}
			{!! Form::select('tag_list[]', $tags, null, ['id' => 'tag_list', 'class' => 'form-control', 'multiple']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Update Article', ['class' => 'btn btn-primary form-control']) !!}
		</div>
				
	{!! Form::close() !!}
	
	@include('errors.list')
	<script type="text/javascript">
		$('#tag_list').select2({
			placeholder: "Select a Tag"
		});
	</script>
	@section('footer')
	
	
@endsection
@stop