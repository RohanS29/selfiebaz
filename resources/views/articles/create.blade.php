<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@extends('app')
	
@section('content')
	<h1>Write a New Article!</h1>
	<hr />
	
	{!! Form::open(['url' => 'articles', 'method' =>'POST', 'file' => 'true', 'enctype' => 'multipart/form-data']) !!}
		<div class="form-group">
			{!! Form::label('title', 'Title:') !!}
			{!! Form::text('title', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('Upload Image', '', ['class' => 'control-label']) !!}
			<input type="file" id="image_path" name="image_path" class="file" accept="image/*" />
		</div>
		<!--
		<div class="form-group">
			{!! Form::label('body', 'Body:') !!}
			{!! Form::textarea('body', null, ['class' => 'form-control']) !!}
		</div>
		-->
		<!-- <div class="form-group">
			{!! Form::label('published_at', 'Publish On:') !!}
			{!! Form::input('date', 'published_at', date('Y-m-d'), ['class' => 'form-control']) !!}
		</div> -->
	
		<div class="form-group">
			{!! Form::label('tag_lists', 'Tags:') !!}
			{!! Form::select('tag_list[]', $tags, null, ['id' => 'tag_list', 'class' => 'form-control', 'multiple']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Add Article', ['class' => 'btn btn-primary form-control']) !!}
		</div>
	{!! Form::close() !!}
	
	<!--<script>tinymce.init({ selector:'textarea' });</script>-->
	
	@include('errors.list')
	
	<script type="text/javascript">
		$('#tag_list').select2({
			placeholder: "Select a Tag" 
		});
	</script>
@stop