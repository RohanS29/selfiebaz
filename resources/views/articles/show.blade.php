@extends('app')

@section('content')
	<div class="content">
		<div class="single-page">
			<div class="print-main">
				<a href="#">{{ $article->title }}</a>
				@if($article->image_path)
					<img src="../uploads/{{$article->image_path}}" alt="$article" class="img-responsive"/>
				@endif
				<p class="ptext">{{ $article->body }}</p>
				<a href="{{ url('/articles/edit', [$article->id]) }}">
					<input type="button" value="Edit" />
				</a>
			</div>
			<br><br>
			@unless ($article->tags->isEmpty())
				<h4>Tags:
					@foreach ($article->tags as $tag)
						{{ $tag->name }}
					@endforeach
				</h4>
			@endunless
		</div>
	</div>
@stop