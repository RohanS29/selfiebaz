<!DOCTYPE html>
<html>
	<head>
		@include('partials.header')
	</head>
	<body>
		@include('partials.nav')
		<div class="container-fluid" style="padding-top: 60px">
			@include('flash::message')
			<script type="text/javascript">
				$('#flash-overlay-modal').modal();
				$('div.alert').not('.alert-important').delay(3000).slideUp(300);
			</script>
			<script type="text/javascript">
				$('#tag_list').select2();
			</script>
			@yield('content')
		</div>
		<div class="footer">
			<div class="container">
				<div class="copyrights">
					<p>Rohan Shambharkar © 2016 All rights reserved</p>
				</div>
			</div>
		</div>
		
	</body>
</html>
