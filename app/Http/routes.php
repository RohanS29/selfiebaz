<?php



Route::group(['middleware' => ['web']], function(){
		
	Route::get('/', function () {
    	return view('welcome');
	});
	
	Route::get('/wall', 'ArticlesController@index');
	Route::get('auth/login', 'Auth\AuthController@index');
    Route::get('auth/register', 'Auth\AuthController@register');
    // //Route::get('auth/create', 'Auth\AuthController@getRegister');
    Route::post('auth/create', 'Auth\AuthController@create');
    // //Route::get('profile', 'ProfileController@updateProfile');
    // Route::get('auth/logout', 'Auth\AuthController@logout');
    Route::post('auth/logout', 'Auth\AuthController@logout');
    Route::get('auth/getlogin', 'Auth\AuthController@getLogin');
    Route::post('auth/getlogin', 'Auth\AuthController@postLogin');
	Route::resource('profiles', 'ProfileController');

	// Route::get('articles', 'ArticlesController@index');
	// Route::get('articles/create', 'ArticlesController@create');
	// Route::get('articles/{id}', 'ArticlesController@show');
	// Route::post('articles', 'ArticlesController@store');
	// Route::patch('articles/{id}', 'ArticlesController@update');
	//Route::post('articles/{id}', 'ArticlesController@destroy');
	Route::resource('articles', 'ArticlesController');
	Route::get('/profile', 'ArticlesController@getProfile');
	
	Route::get('/articles/edit/{id}', 'ArticlesController@edit');
	
	Route::post('/deleteTag', 'TagController@deleteTag');
	
	Route::resource('tags', 'TagController');
	
	// add comment
	Route::post('comment/add','CommentController@store');
	
	// Route::auth();
// 
    // Route::get('/home', 'HomeController@index');
	
	Route::get('foo', ['middleware' => 'manager', function()
	{
		return 'this page may only be viewed by managers!';
	}]);
});

// Route::group(['middleware' => 'web'], function () {
    // Route::auth();
// 
    // Route::get('/home', 'HomeController@index');
// });

