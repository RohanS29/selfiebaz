<?php

namespace App\Http\Controllers;

use App\Article;
use App\User;
use App\Tag;
use App\Comment;
use App\Http\Requests;
use App\Http\Requests\ArticleRequest;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
//use Request;

class ArticlesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth', ['only' => ['create', 'edit']]);
	}
	
    public function index()
	{
		//dd( Auth::user()->name);
		$users = User::all();
		//dd($users);
		$articles = Article::latest('published_at')->published()->get();
		$latest = Article::latest('published_at')->first();
		$comments = Comment::get();
		//dd($comments);
		
		return view('articles.index', compact('articles', 'latest', 'comments', 'users'));		
			
		
	}
	
	public function getProfile()
	{
		
		//dd($articles);
		//
		$tags = Tag::lists('name', 'id');
		if(Auth::user()->admin == 0)
		{
			$articles = Auth::user()->articles()->get();
					
			return view('pages.user', compact('articles', 'latest'));
		}
			
		else {
			$articles = Article::latest('published_at')->published()->get();
			$latest = Article::latest('published_at')->first();
			
			return view('pages.admin', compact('articles', 'latest', 'tags'));
		}
	}
	
	public function show($id)
	{
		$article = Article::findOrFail($id);
		return view('articles.show', compact('article'));
	}
	
	public function create()
	{
		
		$tags = Tag::lists('name', 'id');
		
		/* 
		if(Auth::guest())
		{
			return redirect('articles');
		}
		/*
		 * if the person is guest, laravel will 
		 * redirect the user to the 'articles' page.
		 * i.e., guest will not have access to other 
		 * imp pages, like 'create' in this case
		 * 
		 * */
		return view('articles.create', compact('tags'));
	}
	
	public function store(ArticleRequest $request)
	{
		$finalRequest=$request->all();
		//dd($request->file('image_path'));
      	if ($request->hasFile('image_path')) 
      	{
	      	$destinationPath = 'uploads'; // upload path
      		$extension = $request->file('image_path')->getClientOriginalExtension(); // getting image extension
      		$fileName = rand(11111,99999).'.'.$extension; // renaming image
      		$request->file('image_path')->move($destinationPath, $fileName);
			//dd($request->file('image_path')->move($destinationPath, $fileName));
			$finalRequest['image_path'] = $fileName;
      	}
		//dd($request->all());
      	
		
		//dd($finalRequest['image_path']);	
		$article = Auth::user()->articles()->create($finalRequest);
		//dd($article['image_path']);
		if($request->input('tag_list'))
		{
			$this->syncTags($article, $request->input('tag_list'));
		}
		else {
			$this->syncTags($article, []);
		}	
			
			
		//$article = new Article($request->all());
		//$this->createArticle($request);
		
		//$article->tags()->attach($request->input('tag_list'));
		
		flash('Your post has been successfully created!');
		
		return redirect('articles');
	}
	
	public function edit($id)
	{
		$article = Article::findOrFail($id);
		
		$tags = Tag::lists('name', 'id');
		//dd($article['image_path']);
		
		return view('articles.edit', compact('article', 'tags'));
	}
	
	public function update($id, ArticleRequest $request)
	{
		$article = Article::findOrFail($id);
		//dd($request->tag_list['1']);
		if($request['image_path'])
		{
			$finalRequest=$request->all();
			//dd($request->hasFile('image_path'));
      		if ($request->hasFile('image_path')) 
      		{
		      	$destinationPath = 'uploads'; // upload path
      			$extension = $request->file('image_path')->getClientOriginalExtension(); // getting image extension
      			$fileName = rand(11111,99999).'.'.$extension; // renaming image
      			$request->file('image_path')->move($destinationPath, $fileName);
				//dd($request->file('image_path')->move($destinationPath, $fileName));
      		}
			//dd($request->all());
      		$finalRequest['image_path'] = $fileName;
			$article->update($finalRequest);
		}
		else 
		{
			$request['image_path'] = $article['image_path'];
			$article->update($request->all());
		}
		
		if($request->input('tag_list'))
		{
			$this->syncTags($article, $request->input('tag_list'));
		}
		else {
			$this->syncTags($article, []);
		}
		
		return redirect('articles');
	}
	
	/*
	 * Sync up the list of the tags in the database.
	 * 
	 * */
	
	public function syncTags(Article $article, array $tags)
	{
		$article->tags()->sync($tags);
	}
	
	public function destroy($id)
	{
		//dd($id);
		$article = Article::findOrFail($id);
		$article->delete();
		//dd($article);
		return redirect('articles'); 
	}
	
	// public function createArticle(ArticleRequest $request)
	// {
		// $article = Auth::user()->articles()->create($request->all());
// 		
		// $this->syncTags($article, $request->input('tag_list'));
// 		
		// return $article;
	// }
}
