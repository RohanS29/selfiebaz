<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\RegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/articles';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    
     
    protected function validator(Request $request)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

	protected function index()
    {
        return view('auth.login');
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(Request $request)
    {
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);
		
		if(Auth::attempt(['email' => $request['email'], 'password' => $request['password']])){
			return redirect('/articles');
		}
		
		//return redirect('/wall');
    }
	
	protected function register()
	{
		return view('/auth/register');
	}
	
	public function postLogin(Request $request)
    {

        $this->validate($request, [
                'email' => 'required|email|max:255',
                'password' => 'required|min:6'
            ]);
			
		
        $email=$request['email'];
		// $user = User::where('email', $email)->get();
        // return $user->name;
        $password=$request['password'];
		//dd($password);
		// $attempt = Auth::attempt(['email' => $email, 'password' => $password]);
// 		
		// dd($attempt);
		
        if (Auth::attempt(['email' => $email, 'password' => $password]))
        {
           //Session::put('email', $email);
            //dd( Auth::user()->name);
            return redirect('/articles');
        }
        else
        {
        	//dd($email);
            return view('auth/login');
        }

        // dd($email,$password);
        // dd($password);
     }
	
	 public function logout()
     {
     	Auth::logout();
        return redirect('/');
	 }
	
}
