<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Article;
use App\Tag;
use App\Http\Requests;
use App\Http\Requests\ArticleRequest;
use App\Http\Requests\TagRequest;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;

class CommentController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}
	
	
    /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//dd($request);
		//on_post, from_user, body
		$input['user_id'] = $request->user()->id;
		$input['article_id'] = $request->input('on_post');
		$input['comment'] = $request->input('body');
		//$slug = $request->input('slug');
		Comment::create( $input );
 
		return redirect('/wall'); 	
	}
}
