<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Tag;
use App\Http\Requests;
use App\Http\Requests\ArticleRequest;
use App\Http\Requests\TagRequest;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;

class TagController extends Controller
{
    public function store(TagRequest $request)
	{
		//dd($request);
		$finalRequest = $request->all();
		//dd($finalRequest);
		Tag::create($finalRequest);
		return redirect('profile');
	}
	
	public function deleteTag(Request $request)
	{
		$finalRequest = $request->all();
		//dd($finalRequest['tag_list']);
		$tag_array = $finalRequest['tag_list'];
		$tag_id = $tag_array['0'];
		//dd($tag_id);
		Tag::destroy($tag_id);
		
		return redirect('profile');
	}
	
}
