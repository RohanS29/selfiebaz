<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Article extends Model
{
	protected $fillable = [
   		'title',
   		'published_at',	
   		'user_id',
   		'image_path'
   	];
	
	protected $dates = ['published_at']; 
	
	public function scopePublished($query)
	{
		$query->where('published_at', '<=', Carbon::now());
	}
	
	public function setPublishedAtAttribute($date)
	{
		$this->attributes['published_at'] = Carbon::parse($date);
	}
	
	/*
	 * An article is owned by a user.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 * 
	 * */
	
	public function user()
	{
		return $this->belongsTo('App\User');
	}
	
	/*
	 * Get the tags associated with the given article.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 * 
	 * */
	
	public function tags()
	{
		return $this->belongsToMany('App\Tag')->withTimestamps();
	}
	
	
	/*
	 * A article can have many comments.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 * 
	 * */
	
	public function comments()
	{
		return $this->hasMany('App\Comment');
	}
	
	
	/*
	 * Get a list of tag ids associated with the current article.
	 * 
	 * @return array
	 * 
	 * */
	
	
	public function getTagListAttribute()
	{
		return $this->tags->lists('id');
	}
	
}
