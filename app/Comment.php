<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Comment extends Model
{
    protected $fillable = [
    	'user_id',
    	'article_id',
    	'comment',
    	'published_at',
    ];
	
	/*
	 * A comment is owned by a user.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 * 
	 * */
	
	public function user()
	{
		return $this->belongsTo('App\User');
	}
	
	/*
	 * A single comment belongs to an article.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 * 
	 * */
	 
	 public function articles()
	 {
	 	return $this->belongsTo('App\Article');
	 }
	
}
